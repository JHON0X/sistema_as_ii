


	<!DOCTYPE html>
	<html lang="es" class="no-js">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta charset="UTF-8">
		<title>Comprar</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="css/linearicons.css">
			<link rel="stylesheet" href="css/font-awesome.min.css">
			<link rel="stylesheet" href="css/bootstrap.css">
			<link rel="stylesheet" href="css/magnific-popup.css">
			<link rel="stylesheet" href="css/animate.min.css">
			<link rel="stylesheet" href="css/owl.carousel.css">
			<link rel="stylesheet" href="css/main.css">
			<link rel="stylesheet" href="css/compra.css">
		</head>
		<body style="background: url(img/fondcu.jpg);background-size: cover;">
			
			<section class="banner-area" id="home" style="background: rgba(0,0,0,0.5)">
				
				<header class="default-header">
					<nav class="navbar navbar-expand-lg  navbar-light">
						<div class="container">
							  <a class="navbar-brand" href="index.html">
							  	<img src="img/logo.png" alt="">
							  </a>
							  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							    <span class="text-white lnr lnr-menu"></span>
							  </button>

							  <div class="collapse navbar-collapse justify-content-end align-items-center" id="navbarSupportedContent">
							    <ul class="navbar-nav">
									<li><a href="index.html">INICIO</a></li>
									<li><a href="vender.php">Vender</a></li>
									
									
							    </ul>
							  </div>						
						</div>
					</nav>
				</header>
			</section>
		

			
				<div class="container">
					<div class="row justify-content-center">
							<div class="text-center">
								<h1 class="mb-10" style="color:white; ">Compra los siguientes Tipos de Cueros</h1>
								<div class="row col-lg-12">
									<div class="col-lg-6">
									<img src="img/cueventa.jpg"><br><br>
									<a href="comprar_pre.php" class="btn btn-info col-lg-5">Comprar►</a>
									</div>
									<div class="col-lg-6">
									<img src="img/cuerobla.jpg"><br><br>
									<a href="comprar_proce.php" class="btn btn-info col-lg-5">Comprar►</a>
									</div>
								</div>
						</div>
					</div>					
				</div>
				<div class="container">
					<div class="row justify-content-center">
							<div class="text-center">
								
								<h1 class="mb-10" style="color:white;">Compra los siguientes Tipos de carteras</h1>
								<div class="row col-lg-12">
									<div class="col-lg-6">
									<img src="img/cartera2.jpg"><br><br>
									<a href="comprar_carte.php" class="btn btn-info col-lg-5">Comprar►</a>
									</div>
									<div class="col-lg-6">
									<img src="img/cartera3.jpg"><br><br>
									<a href="comprar_carte.php" class="btn btn-info col-lg-5">Comprar►</a>
									</div>
								</div>
							</div>
						</div>
					</div>					
				</div>

 

	
			
	
			
					

			<script src="js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
			<script src="js/vendor/bootstrap.min.js"></script>
			<script src="js/jquery.ajaxchimp.min.js"></script>
			<script src="js/jquery.magnific-popup.min.js"></script>	
			<script src="js/owl.carousel.min.js"></script>			
			<script src="js/jquery.sticky.js"></script>
			<script src="js/slick.js"></script>
			<script src="js/jquery.counterup.min.js"></script>
			<script src="js/waypoints.min.js"></script>		
			<script src="js/main.js"></script>	
		</body>
	</html>